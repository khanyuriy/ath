# Ath
Данный репозиторий содержит решение тестового задания от ATH.
Решение выполнено с использованием TypeScript + Angular 2.

## Необходимые условия 
Node 4 или более поздняя, NPM 3 или более поздняя.

## Установка
1. npm i -g angular-cli
2. cd /path/to/project/root
3. npm install

## Запуск
4. npm start
5. открыть в браузере страницу http://localhost:4200