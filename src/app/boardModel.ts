import { ITask, IUser, Status, ITasksByStatus } from './domain';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


interface ITaskGroupsByStatus {
	[status: string]: ITasksByExecutor;
}

interface ITasksByExecutor {
	[executorId: string]: ITask[];
}

interface ITasksById {
	[taskId: string]: ITask;
}


export class BoardModel {

	private users: IUser[];

	private tasksGroupsByStatus: ITaskGroupsByStatus = {};
	private tasksById: ITasksById = {};

	private _initialized: boolean = false;

	get isInitialized() : boolean {
		return this._initialized;
	}

	initialize(tasksByStatus: ITasksByStatus , users: IUser[] ) {

		// группируем задачи по статусу и исполнителю
		for (let key in tasksByStatus) {
			tasksByStatus[key].forEach(task => {
				let byStatus = this.tasksGroupsByStatus[key] = this.tasksGroupsByStatus[key] || {};
				let executorKey = this.getExecutorKey(task.ExecutorId);
				let byExecutor = byStatus[executorKey] = byStatus[executorKey] || [];
				byExecutor.push(task);

				this.tasksById[task.Id] = task;
			});
		}

		this.users = users;

		this._initialized = true;
	}

	private getExecutorKey(executorId: string | null) {
		return executorId || 'null';
	}

	getUsers() {
		return this.users;
	}

	getTasks(userId: string, status: Status) {
		let byStatus = this.tasksGroupsByStatus[status];
		return byStatus && byStatus[userId];
	}

	adjustOnUpdateTask(originalTask: ITask, updatedTask: ITask) {
		let byStatus = this.tasksGroupsByStatus[originalTask.Status];
		let executorKey = this.getExecutorKey(originalTask.ExecutorId);
		let executorTasks = byStatus[executorKey];

		// Задача не меняет коллекцию
		if (originalTask.Status == updatedTask.Status && originalTask.ExecutorId == updatedTask.ExecutorId) {
			byStatus[executorKey] = byStatus[executorKey].map(task => task.Id == originalTask.Id ? updatedTask : task);
		} else {
			byStatus[executorKey] = byStatus[executorKey].filter(task => task.Id != originalTask.Id);
			byStatus = this.tasksGroupsByStatus[updatedTask.Status];

			let updatedExecutorKey = this.getExecutorKey(updatedTask.ExecutorId);
			byStatus[updatedExecutorKey] = byStatus[updatedExecutorKey] || [];
			byStatus[updatedExecutorKey] = byStatus[updatedExecutorKey].map(t => t)
			byStatus[updatedExecutorKey].push(updatedTask);
		}
	}

	adjustOnInsertTask(newTask: ITask) {
		let byStatus = this.tasksGroupsByStatus[newTask.Status];
		let executorKey = this.getExecutorKey(newTask.ExecutorId);

		byStatus[executorKey] = byStatus[executorKey] || [];
		byStatus[executorKey] = byStatus[executorKey].map(t => t)
		byStatus[executorKey].push(newTask);
	}

	adjustOnDeleteTask(deletingTask: ITask) {
		let byStatus = this.tasksGroupsByStatus[deletingTask.Status];
		let executorKey = this.getExecutorKey(deletingTask.ExecutorId);

		byStatus[executorKey] = byStatus[executorKey] || [];
		byStatus[executorKey] = byStatus[executorKey].filter(t => t.Id != deletingTask.Id);
	}

	getWorkloadByExecutor(executorId: string) {
		var result = 0;
		for (let key in this.tasksGroupsByStatus) {
			let byStatus = this.tasksGroupsByStatus[key];
			let byExecutor = byStatus[executorId];
			(byExecutor || []).forEach(t => result += t.Workload);
		}
		return result;
	}

	getWorkloadByStatus(status: string) {
		var result = 0;

		let byStatus = this.tasksGroupsByStatus[status];
		for (let executorKey in byStatus) {
			let byExecutor = byStatus[executorKey];
			(byExecutor || []).forEach(t => result += t.Workload);
		}

		return result;
	}
}