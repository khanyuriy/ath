import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ITask, IUser, ITaskSubmitArgs } from './domain';
import { AthService } from './ath.service';
import { BoardModel } from './boardModel';
import { Observable } from 'rxjs/Observable';

@Component({
	selector: 'task',
	templateUrl: './task.component.html',
	styles: [
		`
			form {
				padding: 10px;
				background: white;
				box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
			}
		`
	]
}) 
export class TaskComponent {
	@Input() task: ITask;
	@Input() users: IUser[];
	@Input() top: number;
	@Input() left: number;
	@Output() onTaskSubmitted: EventEmitter<ITaskSubmitArgs> = new EventEmitter<ITaskSubmitArgs>();
	@Output() onDelete: EventEmitter<any> = new EventEmitter<any>();
	@Output() onClose: EventEmitter<any> = new EventEmitter<any>();

	loading: boolean = false;
	error: any;

	constructor() {
	}

	onSubmit(formValue: ITask) {
		this.onTaskSubmitted.next(<ITaskSubmitArgs>{original: this.task, current: formValue});
	}

	deleteClick() {
		this.onDelete.next({task: this.task});
	}

	closeClick() {
		this.onClose.next();
	}

	isDeleteDisabled() {
		return this.task.Id == null;
	}
}