import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HOST, SCOPE_ID } from './config' 
import { IUser, ITask, Status, ITasksByStatus } from './domain';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/from';

@Injectable()
export class AthService {
	

	constructor (private http: Http) {
	}

	getUsers() : Observable<IUser[]> {
		// return this.http.get(`${HOST}\\users\\get\\${SCOPE_ID}`)
			// .map(response => <IUser[]>response.json() );
		return Observable.from([<IUser[]>[{Id: '1', Name: 'Ivan'}, {Id: '2', Name: 'Andrey'}, {Id: '3', Name: 'Mikhail'}] ]);
	}

	getNewTasks(): Observable<ITask[]> {
		return Observable.from([
			<ITask[]>[
				{Id: '1', Name:'do A New', Status: Status.new, ExecutorId: '1'}
			]
		]);
	}

	getInProgressTasks(): Observable<ITask[]> {
		return Observable.from([
			<ITask[]>[
				{Id: '2', Name:'do B InProgress', Status: Status.inProgress, ExecutorId: '2'}
			]
		]);
	}

	getDoneTasks(): Observable<ITask[]> {
		return Observable.from([
			<ITask[]>[
				{Id: '3', Name:'do C Done', Status: Status.done, ExecutorId: '3'}
			]
		]);
	}


	getTasksByStatus(): Observable<ITasksByStatus> {
		return Observable.from([
			<ITasksByStatus> {
				1: [ <ITask> {Id: '1', Name:'do A New', Description: 'descr', Status: Status.new, ExecutorId: '1'} ],
				2: [ <ITask> {Id: '2', Name:'do B InProgress', Status: Status.inProgress, ExecutorId: '2'} ],
				3: [ <ITask> {Id: '3', Name:'check C Done', Status: Status.done, ExecutorId: '3'} ],
			}

		]);
	}

	getTaskById(taskId: string) : Observable<ITask> {
		return Observable.from([
			<ITask>{ Id: '99', Name: ''}
		]);
	}

	postTask(task: ITask) {
		return Observable.from([0]);
	}

	putTask(newTask: ITask) {
		return Observable.from([0]);
	}
}