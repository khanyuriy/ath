import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AthService } from './ath.service';
import { BoardModel } from './boardModel';
import { TaskComponent } from './task.component';
import { ExecutorTasksComponent } from './executorTasks.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    ExecutorTasksComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [AthService, BoardModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
