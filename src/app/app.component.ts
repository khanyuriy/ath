import { Component, OnInit } from '@angular/core';
import { ITask, IUser, Status, ITaskSubmitArgs, Guid } from './domain';
import { Observable } from 'rxjs/Observable';
import { AthService } from './ath.service';
import { BoardModel } from './boardModel';
import 'rxjs/add/observable/combineLatest';

const DIALOG_TOP_OFFSET = 20;
const DIALOG_LEFT_OFFSET = -10;
const ERROR_MESSAGE = 'В ходе выполнения произошла ошибка, попробуйте снова';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'app works!';
	isLoading: boolean = false;
	editingTask: ITask;
	taskDialogTop: number;
	taskDialogLeft: number;


	constructor(private service: AthService, private boardModel: BoardModel) {
	}

	ngOnInit() {
		this.isLoading = true;
		Observable.combineLatest(
				this.service.getTasksByStatus(),
				this.service.getUsers())
			.subscribe(value => {
				console.log(value);
				this.boardModel.initialize(value[0], value[1])
				this.isLoading = false;
			}, _ => alert(ERROR_MESSAGE));
	}

	getStatuses() {
		return Object.keys(Status).filter(s => !isNaN(parseInt(s)));
	}

	getUsers() {
		return this.boardModel && this.boardModel.getUsers();
	}

	getTasks(userId: string, status: string) {
		return this.boardModel && this.boardModel.getTasks(userId, parseInt(status));
	}

	getStatusName(status: any) {
		let s = parseInt(status);
		switch (s) {
			case Status.new:
				return 'Новые';
			case Status.inProgress:
				return 'В работе';
			default:
				return 'Выполненные'
		}
	}

	createTask($event) {
		this.editingTask = <ITask>{};
		this.taskDialogTop = $event.y+DIALOG_TOP_OFFSET;
		this.taskDialogLeft = $event.x-DIALOG_LEFT_OFFSET;
	}

	onSelectTask($event, task) {
		$event.$event.stopPropagation();
		this.editingTask=$event.task;
		this.taskDialogTop = $event.$event.y+DIALOG_TOP_OFFSET;
		this.taskDialogLeft = $event.$event.x+DIALOG_LEFT_OFFSET;
	}

	onTaskSubmit($event) {
		$event.original.Id != null 
			? this.postTask($event.original, $event.current)
			: this.putTask($event.current);
	}

	private postTask(original: ITask, current: ITask) {
		let updatingTask = Object.assign({}, original, current);
		
		this.service.postTask(updatingTask).subscribe(_ => {
			this.boardModel.adjustOnUpdateTask(original, updatingTask);
			this.editingTask = null;
		},  _ => alert(ERROR_MESSAGE));
	}

	private putTask(newTask: ITask) {
		newTask.Id = Guid.MakeNew().ToString();
		this.service.putTask(newTask).subscribe(response => {
			this.boardModel.adjustOnInsertTask(newTask);
			this.editingTask = null;
		},  _ => alert(ERROR_MESSAGE));
	}

	onTaskDelete($event) {
		if (confirm('Вы действительно хотите удалить данную задачу ?')) {
			let task = <ITask>$event.task;
			this.service.deleteTask(task.Id).subscribe(response => {
				this.boardModel.adjustOnDeleteTask(task);
				this.editingTask = null;
			}, _ => alert(ERROR_MESSAGE));
		}
	}

	onCloseTaskDialog() {
		this.editingTask = null;
	}

	onTableClick() {
		this.editingTask = null;
	}

	getWorkloadByExecutor(executorId: string) {
		return this.boardModel.getWorkloadByExecutor(executorId);
	}

	getWorkloadByStatus(status: string) {
		return this.boardModel.getWorkloadByStatus(status);

	}
}
