import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { ITask } from './domain';

interface ITaskClickArgs {
	$event: any;
	task: ITask;
}

@Component({
	selector: 'executor-tasks',
	changeDetection: ChangeDetectionStrategy.OnPush,
	template: `
		<ul>
			<li *ngFor="let task of tasks"><a href="#" (click)="onTaskClick($event, task)">{{task.Name}}</a></li>
		</ul>
	`,
	styles: [`
		ul {
			padding: 0;
		}
		ul li {
			list-style-type: none;
		}
	`]
}) 
export class ExecutorTasksComponent {
	@Input() tasks: ITask[];
	@Output() taskClick: EventEmitter<ITaskClickArgs> = new EventEmitter<ITaskClickArgs>();

	onTaskClick($event: any, task: ITask) {
		this.taskClick.next({$event: $event, task: task});
	}
}