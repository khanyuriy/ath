import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HOST, SCOPE_ID } from './config' 
import { IUser, ITask, Status, ITasksByStatus } from './domain';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/from';

@Injectable()
export class AthService {
	

	constructor (private http: Http) {
	}

	getUsers() : Observable<IUser[]> {
		return this.http.get(`${HOST}\\users\\get\\${SCOPE_ID}`)
			.map(response => <IUser[]>response.json() );
	}

	getTasksByStatus(): Observable<ITasksByStatus> {

		let observables = Object.keys(Status)
			.filter(s => !isNaN(parseInt(s)))
			.map(s => this.http.get(`${HOST}\\tasks\\byStatus\\${SCOPE_ID}\\${s}`));

		return Observable.combineLatest(observables, function() {
			let tasksByStatus = <ITasksByStatus>{};
			for (let i=0; i<arguments.length; i++) {
				let tasks = <ITask[]>arguments[i].json();
				if (tasks.length > 0) {
					tasksByStatus[tasks[0].Status] = tasks;
				}
			}
			return tasksByStatus;
		})
	}

	getTaskById(taskId: string) : Observable<ITask> {
		return Observable.from([
			<ITask>{ Id: '99', Name: ''}
		]);
	}

	postTask(task: ITask) {
		return this.http.post(`${HOST}\\tasks\\${SCOPE_ID}`, task);
	}

	putTask(newTask: ITask) {
		let options = new RequestOptions({
			search: new URLSearchParams(`scopeId=${SCOPE_ID}`)
		});
		return this.http.put(`${HOST}\\api\\Tasks`, newTask, options);
	}

	deleteTask(taskId: string) {
		return this.http.delete(`${HOST}\\tasks\\${SCOPE_ID}\\${taskId}`);
	}
}