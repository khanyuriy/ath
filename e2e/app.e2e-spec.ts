import { AthPage } from './app.po';

describe('ath App', function() {
  let page: AthPage;

  beforeEach(() => {
    page = new AthPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
